﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineMedicine.Models;
using OnlineMedicine.ViewModels;
using System;

namespace OnlineMedicine.Controllers
{
    public class MedicineController : Controller
    {
        private readonly AppDbContext _context;

        public MedicineController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Medicine
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Medicine> medicines = _context.Medicines
                                    .Include(m => m.Category)
                                    .Include(m => m.Country)
                                    .ToList();

            List<Category> categories = _context.Categories
                                        .ToList();

            MedicineListModel model = new MedicineListModel
            {
                Medicines = medicines,
                Categories = categories
            };

            return View(model);
        }

        // GET: Medicine
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(List<int>? CategoryIds, string? SearchKeyword)
        {
            if(SearchKeyword == null || SearchKeyword == string.Empty)
            {
                SearchKeyword = "";
            }
            List<Medicine> medicines = _context.Medicines
                                    .Include(m => m.Category)
                                    .Include(m => m.Country)
                                    .Where(m => CategoryIds == null || CategoryIds.Count <= 0 || CategoryIds.Contains(m.CategoryId))
                                    .Where(m => m.Name.Contains(SearchKeyword)
                                                || m.Descript.Contains(SearchKeyword)
                                                || m.Country.Name.Contains(SearchKeyword))
                                    .ToList();

            List<Category> categories = _context.Categories
                                        .ToList();

            MedicineListModel model = new MedicineListModel
            {
                CategoryIds = CategoryIds,
                Medicines = medicines,
                Categories = categories
            };

            return View(model);
        }
        public IActionResult Details(int? id)
        {
            Medicine m = _context.Medicines.Include(x => x.Country).
                            Include(x => x.Type).Include(x => x.Category).
                            FirstOrDefault(x => x.Id == id);
            ViewBag.m = m;
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult ListAll()
        {
            ViewBag.Medicine = _context.Medicines.Include(x => x.Category).Include(x => x.Type).Include(x => x.Country).ToList();
            return View("List");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.category = _context.Categories.ToList();
            ViewBag.type = _context.Types.ToList();
            ViewBag.country = _context.Countries.ToList();
            return View("Create");
        }

        [HttpPost]
        public IActionResult Create(string name, int category, DateTime expriedDate, string image, string descript, int minAge,
            int typeId, int country, int price, int quantity)
        {
            Medicine m = new Medicine();
            m.Name = name;
            m.CategoryId = category;
            m.ExpiredDate = expriedDate;
            m.Image = image;
            m.Descript = descript;
            m.MinAge = minAge;
            m.TypeId = typeId;
            m.Price = price;
            m.Quantity = quantity;
            m.CountryId = country;
            _context.Medicines.Add(m);
            if (_context.SaveChanges() > 0)
            {
                ViewBag.Medicine = _context.Medicines.Include(x => x.Category).Include(x => x.Type).Include(x => x.Country).ToList();
                return View("List");
            }
            ViewBag.Error = "Create new failed";
            return View("Create");
        }
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int mid)
        {
            Medicine m = _context.Medicines.Include(x => x.Category)
                .Include(x => x.Type).Include(x => x.Country)
                .FirstOrDefault(x => x.Id == mid);
            ViewBag.category = _context.Categories.Where(x => x.Id != m.CategoryId).ToList();
            ViewBag.type = _context.Types.Where(x => x.Id != m.TypeId).ToList();
            ViewBag.country = _context.Countries.Where(x => x.Id != m.CountryId).ToList();
            ViewBag.date = m.ExpiredDate.Value.ToString("yyyy-MM-dd");
            ViewBag.Medicine = m;
            return View("Edit");
        }
        [HttpPost]
        public IActionResult Edit(string name, int category, DateTime expriedDate, string image, string descript, int minAge,
           int typeId, int country, decimal price, int quantity, int mid)
        {
            Medicine m = _context.Medicines.Include(x => x.Category)
                .Include(x => x.Type).Include(x => x.Country).FirstOrDefault(x => x.Id == mid);
            m.Name = name;
            m.CategoryId = category;
            m.ExpiredDate = expriedDate;
            m.Image = image;
            m.Descript = descript;
            m.MinAge = minAge;
            m.TypeId = typeId;
            m.Price = price;
            m.Quantity = quantity;
            m.CountryId = country;
            if (_context.SaveChanges() >= 0)
            {
                m = _context.Medicines.Include(x => x.Category)
               .Include(x => x.Type).Include(x => x.Country).FirstOrDefault(x => x.Id == mid);
                ViewBag.category = _context.Categories.Where(x => x.Id != m.CategoryId).ToList();
                ViewBag.type = _context.Types.Where(x => x.Id != m.TypeId).ToList();
                ViewBag.country = _context.Countries.Where(x => x.Id != m.CountryId).ToList();
                ViewBag.date = m.ExpiredDate.Value.ToString("yyyy-MM-dd");
                ViewBag.Medicine = m;
                return View("Edit");
            }
            return View("Edit");
        }
        public IActionResult Delete(int mid)
        {

            Medicine m = _context.Medicines.FirstOrDefault(x => x.Id == mid);
            m.Quantity = 0;
            if (_context.SaveChanges() >= 0)
            {
                ViewBag.Medicine = _context.Medicines.Include(x => x.Category).Include(x => x.Type).Include(x => x.Country).ToList();
                return View("List");
            }
            ViewBag.Medicine = _context.Medicines.Include(x => x.Category).Include(x => x.Type).Include(x => x.Country).ToList();
            return View("List");

        }
    }
}
